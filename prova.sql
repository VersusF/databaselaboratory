select distinct ie.id,i.nomeins as nome, d.nome as discr, ie.hamoduli, abs(ie.modulo) as modulo,
                            ie.nomemodulo as "nomeModulo", ie.discriminantemodulo as "discriminanteModulo",
                            ie.haunita, ie.nomeunita as "nomeUnita", ie.crediti, p.nome || ' ' || p.cognome as docente
                from inserogato ie join insegn i on ie.id_insegn=i.id
                    join corsostudi cs on ie.id_corsostudi=cs.id
                    left join discriminante d on ie.id_discriminante = d.id
                    left join docenza doc on doc.id_inserogato = ie.id
                    join persona p on doc.id_persona =p.id
                    where cs.id = %s and ie.annoaccademico= %s
                    order by i.nomeins, modulo


SELECT I.nomeins, IE.annoaccademico AS "annoAccademico",
		D.nome AS "discriminante",
		IE.discriminantemodulo AS "discriminanteModulo",
		IE.nomeModulo AS "nomeModulo",
		IE.url,
		IE.lingua,
		P.nome || ' ' || P.cognome,
		PL.abbreviazione AS "periodoLez",
		IE.crediti,
		IE.obiettiviformativi AS "obiettiviFormativi",
		IE.programma,
		IE.modalitaesame AS "ModalitaEsame"
FROM Insegn I
	JOIN InsErogato IE ON IE.id_insegn = I.id
    JOIN insInPeriodo IIP ON IIP.id_insErogato = IE.id
    JOIN periodolez PL ON IIP.id_periodolez = PL.id
    JOIN docenza Doc ON Doc.id_inserogato = IE.id
    JOIN persona P ON Doc.id_persona = P.id
    JOIN discriminante D ON IE.id_discriminante = D.id
WHERE
    IE.id = 81177



/*
nome, (da join con insegn)
                annoaccademico
                discriminante, (da join con discriminante)
                discriminante modulo
                numeroModulo
                url
                lingua
                Lista docenti(da join con docenza e persona per avere nome e cognome)
                periodo lezione
                crediti
                obiettivi formativi
                programma
                modalita esame*/