from flask import Flask, request, render_template
import psycopg2
from datetime import datetime

app = Flask(__name__)


@app.route('/prenota', methods=['GET', 'POST'])
def prenota():
    params = request.form if request.method == 'POST' else request.args
    codiceFiscale = params['codiceFiscale']
    iataCompagnia = params['iataCompagnia']
    numeroVolo = int(params['numeroVolo'])
    orarioVolo = datetime.strptime(params['orarioVolo'], '%Y-%m-%d %H:%M')
    isBusiness = params['isBusiness'] == '1'
    # Connessione alla base di dati
    conn = psycopg2.connect(host='databaseX', database='X', user='admin',
                            cursor_factory=psycopg2.extras.DictCursor,
                            password='admin')
    query_posti_liberi = '''
        SELECT % - %
        FROM Volo
        WHERE iatacompagnia = %
            AND numero = %
            AND orarioPartenza = %
    '''
    tupla_posti_liberi = ()
    if isBusiness:
        tupla_posti_liberi = ('postibusiness', 'postibusinesscomprati')
    else:
        tupla_posti_liberi = ('postieconomy', 'postieconomycomprati')
    tupla_posti_liberi += (iataCompagnia, numeroVolo, orarioVolo) 
    with conn:
        conn.isolationlevel = 'REPEATABLE READ'
        # VERIFY EMPTY SEATS
        with conn.cursor() as cur:
            cur.execute(query_posti_liberi, tupla_posti_liberi)
            if cur.fetchone()[0] > 0:
                # BOOK A SEAT
                cur.execute('...')
    conn.close()
    # Termine
    return render_template('prenotazioneEffettuata.html')


app.run(debug=True)
