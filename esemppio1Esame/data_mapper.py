import psycopg2
import psycopg2.extras


_database = None


def startup(user, password):
    global _database
    _database = psycopg2.connect(host='dbserver.scienze.univr.it', user=user,
                                 database='id091jne', password=password)
    _database.autocommit = True


def shutdown():
    _database.close()


def _get_cursor():
    return _database.cursor(cursor_factory=psycopg2.extras.DictCursor)


def add_belltower(citta, altezza=None, anno=None):
    print('inserisco', citta, altezza, anno)
    with _get_cursor() as cur:
        if altezza and anno:
            cur.execute('''INSERT INTO campanile(citta, altezza, anno)
                        VALUES(%s, %s, %s);''', (citta, altezza, anno))
        elif altezza:
            cur.execute('''INSERT INTO campanile(citta, altezza)
                        VALUES(%s, %s);''', (citta, altezza))
        elif anno:
            cur.execute('''INSERT INTO campanile(citta, anno)
                        VALUES(%s, %s);''', (citta, anno))
        else:
            cur.execute('INSERT INTO campanile(citta) VALUES(%s);', (citta,))
        print(cur.statusmessage)
    print('campanile inserito')


def get_campanili():
    result = None
    with _get_cursor() as cur:
        cur.execute('''
                    SELECT C.citta, C.altezza, C.anno, COUNT(campana.id), C.id
                    FROM campanile C LEFT OUTER
                        JOIN campana ON campana.campanile = C.id
                    GROUP BY C.citta, C.altezza, C.anno, C.id;
                    ''')
        result = list(cur.fetchall())
    return result


def add_fusor(nome, anno=None):
    print('inserisco', nome, anno)
    with _get_cursor() as cur:
        if anno:
            cur.execute('''INSERT INTO fonditore(nome, anno_fondazione)
                        VALUES(%s, %s);''', (nome, anno))
        else:
            cur.execute('INSERT INTO fonditore(nome) VALUES(%s);', (nome,))
        print(cur.statusmessage)
    print('fonditore inserito')


def get_elenco_campanili():
    result = None
    with _get_cursor() as cur:
        cur.execute('''
                    SELECT citta, id
                    FROM campanile
                    ''')
        result = list(cur.fetchall())
    return result


def get_elenco_fonditori():
    result = None
    with _get_cursor() as cur:
        cur.execute('''
                    SELECT nome, id
                    FROM fonditore
                    ''')
        result = list(cur.fetchall())
    return result


def get_elenco_note():
    result = None
    with _get_cursor() as cur:
        cur.execute('''
                    SELECT nota
                    FROM elencoNote
                    ''')
        result = list(cur.fetchall())
    return map(lambda x: x[0], result)


def add_bell(numero=None, peso=None, anno=None, nota=None, ottava=None,
             campanile=None, fonditore=None):
    query_string = '''INSERT INTO campana(numero, peso, anno, nota, ottava,
                      campanile, fonditore) VALUES (%s, %s, %s, %s, %s, %s,
                      %s)'''
    tupla = (numero, peso, anno, nota, ottava, campanile, fonditore)
    with _get_cursor() as cur:
        cur.execute(query_string, tupla)
        print(cur.statusmessage)


def get_belltower_details(belltower_id):
    campanile, campane = None, None
    _database.isolation_level = 'REPEATABLE READ'
    _database.autocommit = False
    belltower_query_string = '''
        SELECT citta, altezza, anno
        FROM campanile
        WHERE id = %s'''
    bells_query_string = '''
        SELECT C.numero, C.peso, C.ottava, C.nota, C.anno, F.nome as fonditore
        FROM campana C JOIN fonditore F ON C.fonditore = F.id
        WHERE C.campanile = %s
        ORDER BY C.numero
        '''
    with _database:
        with _get_cursor() as cur:
            cur.execute(belltower_query_string, (belltower_id,))
            campanile = cur.fetchone()
            cur.execute(bells_query_string, (belltower_id,))
            campane = cur.fetchall()
    _database.isolation_level = 'DEFAULT'
    _database.autocommit = True
    return campanile, campane
