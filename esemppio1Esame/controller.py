from flask import Flask, render_template, request, redirect
from getpass import getpass
from decimal import Decimal
import data_mapper


app = Flask(__name__)


@app.route('/')
def main():
    campanili = data_mapper.get_campanili()
    return render_template('index.html', campanili=campanili)


@app.route('/add_belltower/')
def add_belltower():
    return render_template('add_belltower.html', message_error='')


@app.route('/belltower_added', methods=['POST'])
def belltower_added():
    citta = request.form['citta']
    altezza = None
    anno = None
    try:
        altezza_str = request.form['altezza']
        altezza = int(altezza_str) if altezza_str != '' else None
        anno_str = request.form['anno']
        anno = int(anno_str) if anno_str != '' else None
    except ValueError:
        return render_template('add_belltower.html',
                               message_error='Dati non validi')
    data_mapper.add_belltower(citta, altezza=altezza, anno=anno)
    return render_template('add_belltower.html',
                           message_error='Campanile aggiunto')


@app.route('/add_fusor')
def add_fusor():
    return render_template('add_fusor.html', message_error='')


@app.route('/fusor_added', methods=['POST'])
def fusor_added():
    nome = request.form['nome']
    anno = None
    try:
        anno_str = request.form['anno']
        anno = int(anno_str) if anno_str != '' else None
    except ValueError:
        return render_template('add_fusor.html',
                               message_error='Dati non validi')
    data_mapper.add_fusor(nome, anno=anno)
    return render_template('add_fusor.html',
                           message_error='fonditore aggiunto')


@app.route('/add_bell')
def add_bell(message_error=''):
    elenco_campanili = data_mapper.get_elenco_campanili()
    elenco_fonditori = data_mapper.get_elenco_fonditori()
    elenco_note = data_mapper.get_elenco_note()
    return render_template('add_bell.html', elenco_fonditori=elenco_fonditori,
                           elenco_campanili=elenco_campanili,
                           elenco_note=elenco_note, elenco_ottave=range(1, 10),
                           message_error=message_error)


@app.route('/bell_added', methods=['POST'])
def bell_added():
    numero = int(request.form['numero'])
    peso = Decimal(request.form['peso'])
    anno = int(request.form['anno'])
    nota = request.form['nota']
    ottava = request.form['ottava']
    campanile = int(request.form['campanile'])
    fonditore = int(request.form['fonditore'])
    data_mapper.add_bell(numero=numero, peso=peso, anno=anno, nota=nota,
                         ottava=ottava, campanile=campanile,
                         fonditore=fonditore)
    return add_bell(message_error='campana aggiunta')


@app.route('/belltower_details', methods=['GET'])
def belltower_details():
    belltoer_id = int(request.args['belltower'])
    campanile, campane = data_mapper.get_belltower_details(belltoer_id)
    return render_template('belltower_details.html', campanile=campanile,
                           campane=campane)


def startup():
    username = str(input('Username: '))
    password = str(getpass())
    data_mapper.startup(username, password)
    app.run(debug=True)


if __name__ == '__main__':
    try:
        startup()
    except:
        data_mapper.shutdown()
