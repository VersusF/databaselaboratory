create domain nota AS VARCHAR
    CHECK (value in ('DO', 'DO#', 'RE', 'MIb', 'MI', 'FA', 'FA#', 'SOL', 'LAb', 'LA', 'SIb', 'SI'));

create domain ottava AS INTEGER
    CHECK (value BETWEEN 1 AND 8);

create table campanile(
    id SERIAL primary key,
    citta VARCHAR(30) NOT NULL,
    altezza NUMERIC(5, 2),
    anno INTEGER CHECK(anno > 0)
);

create table fonditore(
    id SERIAL primary key,
    nome VARCHAR(30) NOT NULL,
    anno_fondazione INTEGER CHECK(anno_fondazione > 0)
);

create table campana(
    id SERIAL primary key,
    peso NUMERIC(10,2) NOT NULL,
    nota nota NOT NULL,
    ottava ottava NOT NULL,
    numero INTEGER CHECK(numero BETWEEN 0 AND 9),
    campanile SERIAL REFERENCES campanile,
    fonditore SERIAL REFERENCES fonditore
);

create table ElencoNote(
    nota nota primary key
);

insert into ElencoNote values
    ('DO'),
    ('DO#'),
    ('RE'),
    ('MIb'),
    ('MI'),
    ('FA'),
    ('FA#'),
    ('SOL'),
    ('LAb'),
    ('LA'),
    ('SIb'),
    ('SI')
;