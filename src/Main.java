import java.sql.*;

public class Main {
    public static void main(String[] args){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Postgresql driver not found");
            return;
        }
        System.out.println("Starting connection");
        String url = "jdbc:postgresql://dbserver.scienze.univr.it:5432/id091jne";
        String username = "id091jne";
        String str1 = "o", str2 = "del", str3 = "nif", str4 = "8-";
        String password = str2 + new StringBuilder(str3).reverse().toString() + str1 + new StringBuilder(str4).reverse().toString();
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            System.out.println("Now print bell towers list");
            String query = "SELECT C.citta, C.anno, C.altezza, COUNT(B.id) AS numero_campane " +
                    "FROM campanile C LEFT OUTER JOIN campana B ON C.id = B.campanile " +
                    "GROUP BY C.citta, C.anno, C.altezza " +
                    "ORDER BY C.citta";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery();
            System.out.println("=====================================================");
            while (result.next()) {
                System.out.println("Città: " + result.getString("citta"));
                System.out.println("Anno: " + result.getInt("anno"));
                System.out.println("Altezza: " + result.getInt("altezza"));
                System.out.println("Numero Campane: " + result.getInt("numero_campane"));
                System.out.println("=====================================================");
            }
            System.out.println("Closing connection");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
