# APPUNTI BASI DI DATI LABORATORIO

Piccolo file di appunti per l'esame di basi di dati laboratorio. Gli argomenti sono:

- [Transazioni](#Transazioni)
- [Python: json e csv](##Python:-json-e-csv)
- [Python: psycopg2](##Python:-psycopg2)
- [Python: Flask](##Python:-Flask)
- [HTML: Jinja2](##HTML:-Jinja2)
- [Java: JDBC](##Java:-JDBC)

## Transazioni

Le transazioni non devono interferire l'una con l'altra, quindi è opportuno mantenere il giusto livello di isolamento. I 4 livelli sono:
- `READ UNCOMMITTED`, che però non è implementato in postgresql
- `READ COMMITTED`, livello di default per le transazioni
- `REPEATABLE READ`, I dati letti non cambieranno
- `SERIALIZABLE`, completo isolamento

In sql l'istruzione per selezionare il livello di isolamento è:

```SQL
SET TRANSACTION ISOLATION LEVEL [...]
```

Il livello **__`READ COMMITTED`__** vede tutti i dati aggiornati all'ultimo `COMMIT`, per questo motivo soffre di due anomalie. La prima è la **non-repeatable reads**, Cioè quando un'operazione che dovrebbe essere fatta viene annullata poiché le tuple interessate sono state modificate da un'altra transazione.
La seconda anomalia è il **phantom reads**, che avviene quando vengono effettuate due letture identiche fra loro ma che danno due risultati diversi poiché inframezzati dal commit di un'altra transazione.

Il livello **__`REPEATABLE READ`__** invece garantisce che due letture identiche diano risultati identici. Se su un dato è stato aggiornato e viene eseguito il commit l'operazione che lo voleva modificare (`UPDATE` o `DELETE`) viene bloccata con un messaggio di errore. Questo blocca i *non-repatable reads*, ma mantiene i **phantom-reads** nei casi in cui c'è un `INSERT` con `SELECT`. (Se invece c'è un `UPDATE` con `SELECT` questo livello di isolamento è sufficiente)

Il livello **__`SERIALIZABLE`__** garantisce il completo e massimo isolamento tra le transazioni. Corregge anche l'anomalia di *phantom-reads*, ma alcune transazioni potrebbero venir bloccate e quindi devono essere ripetute. 

Livello-Concorrenza | non-repeatable reads | phantom reads
--- | --- | ---
Read committed | X | X
Repeatable read | | X
Serializable | |


## Python: json e csv
## Python: psycopg2
## Python: Flask
## HTML: Jinja2
## Java: JDBC