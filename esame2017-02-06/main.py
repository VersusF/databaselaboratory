from flask import Flask, render_template, request
from datetime import datetime


app = Flask(__name__)


@app.route('/')
def main():
    citta = [
        {'nome': 'Verona', 'codice': 1},
        {'nome': 'Padova', 'codice': 2},
        {'nome': 'Vicenza', 'codice': 3},
        {'nome': 'Mantova', 'codice': 4}
    ]
    return render_template('index.html', citta=citta)


@app.route('/telefonateCittaData', methods=['GET'])
def telefonateCittaData():
    citta = int(request.args['citta'])
    data = datetime.strptime(request.args['data'], '%Y-%m-%d')
    return str(citta) + ' ' + data.isoformat()
    # lista = model.Clienti(citta, data)
    # if len(lista) == 0:
    #     return render_template('erroreParametri.html')
    # for item in lista:
    #     item['cognome'] = item['cognome'].lower()
    # return render_template('view.html', lista=lista)


app.run(debug=True)
