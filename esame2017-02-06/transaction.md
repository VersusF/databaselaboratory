# TRANSAZIONI

Se le operazioni di update sono eseguite con una sola istruzione, non c'è bisogno di usare transazioni in quanto la singola operazione è isolata.

Se però l'operazione di update è costituita da più istruzioni (per esempio delle `SELECT` e degli `UPDATE`) allora è necessario racchiuderle in una transazione, per garantirne l'atomicità.

Queste transazioni potrebbero avvenire simultaneamente e, a causa dell'esecuzione concorrente, avere effetto l'una sull'altra.
Per esempio ci potrebbe essere una perdita di update, quando entrambe leggono i dati, una delle due committa la modifica, l'altra modifica i dati precedenti alla modifica e aggiorna la base di dati sovrascrivendo i risultati della prima.

Per impedire questo il livello di isolamento minimo richiesto è quello di `SERIALIZABLE`, in quanto una query deve fare una update basata su una select:
``` text
"azzera le durate di alcune telefonate che soddisfano certi requisiti, basati anche sulla durata stessa"
```
Per questo motivo si potrebbero verificare casi di Phantom-reads, e il livello `SERIALIZABLE` è l'unico che protegge le tranzasioni da questo problema.