from flask import Flask

inizio = '''<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Fiaschetta di quiz</title>
    <link type="text/llcss" rel="stylesheet" href="static/styles.css">
</head>
<body>
    <h1>GIOCO DELLE DOMANDE</h1>
'''

fine = '''
    <form action="/selectedTrue" method="GET">
        <input type="submit" id="trueButton" value="Vero">
    </form>
    <form action="/selectedFalse" method="GET">
        <input type="submit" id="falseButton" value="Falso">
    </form>
</body>
</html>'''

fineSconfitta = '''
    <p>Hai perso!!!</p>
    <form action="/selectedBack" method="GET">
        <input type="submit" id="falseButton" value="Torna">
    </form>
    </body>
    </html>
'''

fineVittoria = '''
    <p>Hai vinto!!!</p>
    <form action="/selectedBack" method="GET">
        <input type="submit" id="trueButton" value="Torna">
    </form>
    </body>
    </html>
'''

domande = [
    ('Turing pubblicò "On computable number" nel 1936:', True),
    ('La cardinalità di N è minore di una funzione N -> N', False),
    ('Il potenziale elettrico è definito a meno di una costante', True)
]

i=0
app = Flask("myApp")

@app.route('/')
def inizializza():
    '''Inizializzo la logica di gioco'''
    if (i == len(domande)):
        return inizio + fineVittoria
    return inizio + '<p>' + domande[i][0] + '</p>' + fine

@app.route('/selectedTrue')
def premutoVero():
    return checkAnswer(True)

@app.route('/selectedFalse')
def premutoFalso():
    return checkAnswer(False)

@app.route('/selectedBack')
def premutoIndietro():
    global i
    i=0
    return inizializza()

def checkAnswer(rispostaData):
    global i
    print(str(rispostaData))
    if (rispostaData == domande[i][1]):
        i+=1
        return inizializza()
    else:
        return haiPerso()

def haiPerso():
    return inizio + fineSconfitta

app.run(debug=True)