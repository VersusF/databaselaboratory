from flask import Flask, render_template, request
import psycopg2

app = Flask(__name__)


@app.route('/')
def main():
    biblioteche = [
        {'nome': 'Oppeano', 'id': 0},
        {'nome': 'Bovolone', 'id': 1},
        {'nome': 'Isola Rizza', 'id': 2},
        {'nome': 'Zevio', 'id': 3},
        {'nome': 'Ronco', 'id': 4},
    ]
    return render_template('form.html', biblioteche=biblioteche)


@app.route('/prestitiUtente', methods=['GET'])
def prestitiUtente():
    id_biblioteca = request.args['biblioteca']
    codice_fiscale = request.args['codiceFiscale']
    query_string = '''
        SELECT idRisorsa, dataInizio, durata
        FROM prestito
        WHERE idBiblioteca = %
            AND idUtente = %
    '''
    query_tuple = (id_biblioteca, codice_fiscale)
    return query_string + '\n' + str(query_tuple)
    # conn = psycopg2.connect(database='X')
    # lista = []
    # with conn:
    #     with conn.cursor() as cur:
    #         cur.execute(query_string, query_tuple)
    #         lista = list(cur)
    # conn.close()
    # if len(lista) > 0:
    #     return render_template('view.html', lista=lista)
    # else:
    #     return render_template('nessunaPrenotazione.html')


app.run(debug=True)
